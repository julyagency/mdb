[![banner](demo/imgs/banner.jpg)](#)


## Getting started

Copy folder dist to your project:
```html
dist/
├── css/
│ ├── bootstrap-material-design.css
│ ├── bootstrap-material-design.css.map
│ ├── bootstrap-material-design.min.css
│ ├── bootstrap-material-design.min.css.map
│ ├── ripples.css
│ ├── ripples.css.map
│ ├── ripples.min.css
│ ├── ripples.min.css.map
├── fonts/
│ ├── ArialRoundedMTStd.woff
│ ├── ArialRoundedMTStd.woff2
├── js/
│ ├── material.js
│ ├── material.min.js
│ ├── material.min.js.map
│ ├── ripples.js
│ ├── ripples.min.js
│ ├── ripples.min.js.map
```
Add to `<head>`:
```html
<!-- Mobile support -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Material Design fonts -->
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<!-- Bootstrap -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<!-- Bootstrap Material Design -->
<link rel="stylesheet" href="dist/css/bootstrap-material-design.css">
<link rel="stylesheet" href="dist/css/ripples.min.css">

<!-- Snackbar -->
<link rel="stylesheet" href="//fezvrasta.github.io/snackbarjs/dist/snackbar.min.css">
```

Before the end of `<body>` paste:
```html
<!-- jQuery -->
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>

<!-- Bootstrap -->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<!-- Autosize -->
<script src="//cdnjs.cloudflare.com/ajax/libs/autosize.js/4.0.0/autosize.min.js"></script>

<!-- Snackbar -->
<script src="//fezvrasta.github.io/snackbarjs/dist/snackbar.min.js"></script>

<!-- Bootstrap Material Design -->
<script src="dist/js/ripples.min.js"></script>
<script src="dist/js/material.min.js"></script>
<script>
  $(function () {
    $.material.init();
  });
</script>
```