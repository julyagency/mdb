var extend = require('util')._extend;

function addScenario(group, opts) {
  var defaults = {
    "label": group,
    "url": "http://localhost:8040/#" + group,
    "selectors": ["#" + group + " .page-group"],
    "selectorExpansion": true,
    "misMatchThreshold" : 0.1,
    "requireSameDimensions": true
  };

  return extend(defaults, opts || {});
}

module.exports = {
  "id": "backstop_default",
  "viewports": [
    {
      "label": "phone",
      "width": 571, // .page-group width: 270px (width of normal bootstrap site 300px)
      "height": 4000
    },
    {
      "label": "tablet",
      "width": 1051, // .page-group width: 570px (width of normal bootstrap site 600px)
      "height": 3000
    }
  ],
  "onBeforeScript": "chromy/onBefore.js",
  "onReadyScript": "chromy/onReady.js",
  "scenarios": [
    addScenario('checkbox'),
    addScenario('radio-button'),
    addScenario('toggle-button'),
    addScenario('buttons'),
    addScenario('pagination'),
    addScenario('progress-bar'),

    addScenario('form-type'),

    addScenario('input'),
    addScenario('input', {
      "label": "input Focused",
      "evaluate": "$('#input .form-group').addClass('is-focused')"
    }),

    addScenario('textarea'),
    addScenario('textarea', {
      "label": "textarea Focused",
      "evaluate": "$('#textarea .form-group').addClass('is-focused')"
    }),

    addScenario('fileinput'),
    addScenario('fileinput', {
      "label": "fileinput Focused",
      "evaluate": "$('#fileinput .form-group').addClass('is-focused')"
    }),

    addScenario('select'),
    addScenario('select', {
      "label": "select Focused",
      "evaluate": "$('#select .form-group').addClass('is-focused')"
    }),

    addScenario('card'),
    addScenario('dialog'),
    addScenario('panel')
  ],
  "paths": {
    "bitmaps_reference": "backstop_data/bitmaps_reference",
    "bitmaps_test": "backstop_data/bitmaps_test",
    "engine_scripts": "backstop_data/engine_scripts",
    "html_report": "backstop_data/html_report",
    "ci_report": "backstop_data/ci_report"
  },
  "report": ["browser"],
  "engine": "chrome",
  "engineFlags": [],
  "asyncCaptureLimit": 5,
  "asyncCompareLimit": 50,
  "debug": false,
  "debugWindow": false
};

